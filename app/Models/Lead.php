<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Notifications\Notifiable;

/**
 * Class Lead.
 *
 * @package namespace SOV\Models;
 */
class Lead extends Model implements Transformable
{
    use Notifiable;
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cpfcnpj',
        'name',
        'email',
        'phone'
    ];
}
