<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LeadRepository.
 *
 * @package namespace SOV\Repositories;
 */
interface LeadRepository extends RepositoryInterface
{
    //
}
