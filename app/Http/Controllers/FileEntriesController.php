<?php

namespace App\Http\Controllers;

use App\Models\FileEntry;
use App\Models\Lead;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use App\Notifications\LeadNotification;

class FileEntriesController extends Controller
{
    public function index()
    {
        $files = FileEntry::all();

        return view('files.index', compact('files'));
    }

    public function create()
    {
        return view('files.create');
    }

    public function uploadFile(Request $request)
    {
        $this->validate($request, [
        // 'file' => 'image|max:3000'
        ]);

        $file     = Input::file('file');
        $filename = $file->getClientOriginalName();
        $path     = hash( 'sha256', time());

        if(Storage::disk('uploads')->put($path.'/'.$filename,  File::get($file)))
        {

            $input['filename'] = $filename;
            $input['mime']     = $file->getClientMimeType();
            $input['path']     = $path;
            $input['size']     = $file->getClientSize();
            $file              = FileEntry::create($input);
            $fileStatus        = $file->id;

            /*** abri e ler ***/
            $file           = fopen(storage_path('files/uploads/'.$path.'/'.$filename), 'r');
            $header         = fgetcsv($file);
            $escapedHeader  = [];

            /*** Validar ***/
            foreach ($header as $key => $value)
            {
                $lheader     = strtolower($value);
                $escapedItem = preg_replace('/[^a-z]/', '', $lheader);
                array_push($escapedHeader, $escapedItem);
            }

            /*** loop através de outras colunas ***/
            while($columns=fgetcsv($file))
            {
                if($columns[0]=="")
                {
                    continue;
                }

                $data= array_combine($escapedHeader, $columns);

                /*** Atualização da tabela ***/
                $cpfcnpj  = $data['cpfcnpj'];
                $name     = $data['name'];
                $email    = $data['email'];
                $phone    = $data['phone'];

                $lead          = Lead::firstOrNew(['cpfcnpj' =>$cpfcnpj,'name' =>$name]);
                $lead->email   = $email;
                $lead->phone   = $phone;

                /*** Passando parametro para notification ***/
                if($lead->save())
                {
                    $lead->notify(new LeadNotification());
                }
            }
        }

        /*** Retorno da mensagem de sucesso ***/
        return response()->json(
            [
                'success' => true,
                'id'      => $fileStatus
            ], 200
        );

    }
}
